# Plant-a-DevOps


# Ansible Lab.
In this lab you'll learn to use Ansible and play your first playbook. You have a short description of Ansible and a getting started section
with steps to perform in order to prepare your environment for the lab.

## What is Ansible.
Ansible is an open-source automation tool, or platform, used for IT tasks such as configuration management,
application deployment, intraservice orchestration and provisioning. Automation is crucial these days, with IT environments
that are too complex and often need to scale too quickly for system administrators and developers to keep up if they had to do everything manually.
Automation simplifies complex tasks, not just making developers’ jobs more manageable but allowing them to
focus attention on other tasks that add value to an organization. In other words, it frees up time and increases efficiency.
And Ansible, as noted above, is rapidly rising to the top in the world of automation tools. Let’s look at some of the reasons for Ansible’s popularity.

## Advantages.
* Free. Ansible is an open-source tool.
* Very simple to set up and use. No special coding skills are necessary to use Ansible’s playbooks (more on playbooks later).
* Powerful. Ansible lets you model even highly complex IT workflows.
* Flexible. You can orchestrate the entire application environment no matter where it’s deployed. You can also customize it based on your needs.
* Agentless. You don’t need to install any other software or firewall ports on the client systems you want to automate.
    You also don’t have to set up a separate management structure.
* Efficient. Because you don’t need to install any extra software, there’s more room for application resources on your server.

## Playbooks.
Ansible playbooks are like instruction manuals for tasks. They are simple files written in YAML, which stands for YAML Ain’t Markup Language,
a human-readable data serialization language. Playbooks are really at the heart of what makes Ansible so popular because they
describe the tasks to be done easily and without the need for the user to know or remember any special syntax. Not only can they declare
configurations, but they can orchestrate the steps of any manually ordered task, and can execute tasks at the same time or at different times.

Each playbook is composed of one or multiple plays, and the goal of a play is to map a group of hosts to well-defined roles, represented by tasks.

## Getting started with the virtual environment.

1. Execute `Vagrantfile` to build the master and node virtual machines.
   * `vagrant up` to build/start the vm's.
   * `vagrant halt` to stop the vm's.
   * `vagrant destroy` to remove all the vm's.
   * You can build/start/stop/destroy individual vm's by adding an argument to the command. ex: `vagrant up <argument>`.
        where the argument is the name of the vm.

2. Connect to each vm using `vagrant ssh <argument>` and run the `script-ssh-sync.sh` file. This script will replace the original shs keys inside vm's.
3. Exit vm's and run the `1-sync-private-keys.ssh` file. This script will replace de ssh private keys of the vm's.

Vagrant cheatsheet.[https://gist.github.com/wpscholar/a49594e2e2b918f4d0c4]

## Getting started with the ansible.
After connecting to `master` we need to execute `ansible -m ping all` to make sure all the nodes are reachable.

1. `ansible-playbook playbook.yml` to play your ansible playbook.
2. You can use `ansible-lint` to run a detail check of your playbooks before you execute them.
    For example, if you run ansible-lint on the `single_playbook.yml` playbook, you’ll get the following results:
    ```bash
    [ANSIBLE0010] Package installs should not use latest
    single_playbook.yml:8
    Task/Handler: Installs Apache Web Server

    [ANSIBLE0010] Package installs should not use latest
    single_playbook.yml:12
    Task/Handler: Installs Apache Web Server

    [ANSIBLE0013] Use shell only when shell functionality is required
    single_playbook.yml:16
    Task/Handler: httpd service enable and start centos

    [ANSIBLE0013] Use shell only when shell functionality is required
    single_playbook.yml:23
    Task/Handler: httpd service enable and start Ubuntu

    .....
    ```
3. `ansible-playbook main.yml -t php-demo-run` will execute the main file but only the tag specified in.

### Ansible roles.
Roles are ways of automatically loading certain vars_files, tasks, and handlers based on a known file structure.
Grouping content by roles also allows easy sharing of roles with other users.

Role Directory Structure:
```bash
site.yml
webservers.yml
fooservers.yml
roles/
   common/
     tasks/
     handlers/
     files/
     templates/
     vars/
     defaults/
     meta/
   webservers/
     tasks/
     defaults/
     meta/
```

Roles expect files to be in certain directory names. Roles must include at least one of these directories,
however it is perfectly fine to exclude any which are not being used. When in use,
each directory must contain a `main.yml` file, which contains the relevant content:

* `tasks` - contains the main list of tasks to be executed by the role.
* `handlers` - contains handlers, which may be used by this role or even anywhere outside this role.
* `defaults` - default variables for the role (see Using Variables for more information).
* `vars` - other variables for the role (see Using Variables for more information).
* `files` - contains files which can be deployed via this role.
* `templates` - contains templates which can be deployed via this role.
* `meta` - defines some meta data for this role.

## Docker Lab.

Same as Ansible, build a vagrant environment then execute script `1-install-docker.sh` to install Docker on the VM.
After Docker is installed you have to add user to docker group or else you wont be able to run docker commands without `sudo`.

```bash
groupadd docker
usermod -aG docker $USER
```

### Docker concepts.
Docker is a platform for developers and sysadmins to develop, deploy, and run applications with containers. The use of Linux containers to deploy applications is called containerization. Containers are not new, but their use for easily deploying applications is.

Containerization is increasingly popular because containers are:

* Flexible: Even the most complex applications can be containerized.
* Lightweight: Containers leverage and share the host kernel.
* Interchangeable: You can deploy updates and upgrades on-the-fly.
* Portable: You can build locally, deploy to the cloud, and run anywhere.
* Scalable: You can increase and automatically distribute container replicas.
* Stackable: You can stack services vertically and on-the-fly.

### Images and containers
A container is launched by running an image. An image is an executable package that includes everything needed to run an application--the code,
a runtime, libraries, environment variables, and configuration files.
A container is a runtime instance of an image--what the image becomes in memory
when executed (that is, an image with state, or a user process). You can see a list of your running
containers with the command, docker ps, just as you would in Linux.

### Containers and virtual machines
A container runs natively on Linux and shares the kernel of the host machine with other containers.
It runs a discrete process, taking no more memory than any other executable, making it lightweight.

By contrast, a virtual machine (VM) runs a full-blown “guest” operating system with virtual access to host
resources through a hypervisor. In general, VMs provide an environment with more resources than most applications need.

Docker environment            |  Virtual Machine environment
:-------------------------:|:-------------------------:
![Image description](docs/Container@2x.png) |  ![Image description](docs/VM@2x.png)

### Test Docker installation

1. Test that your installation works by running the simple Docker image, hello-world:

```bash
docker run hello-world

Unable to find image 'hello-world:latest' locally
latest: Pulling from library/hello-world
ca4f61b1923c: Pull complete
Digest: sha256:ca0eeb6fb05351dfc8759c20733c91def84cb8007aa89a5bf606bc8b315b9fc7
Status: Downloaded newer image for hello-world:latest

Hello from Docker!
This message shows that your installation appears to be working correctly.
...
```
2. List the hello-world image that was downloaded to your machine:

```bash
docker image ls
```

3. List the hello-world container (spawned by the image) which exits after displaying its message.
If it were still running, you would not need the --all option:

```bash
docker container ls --all

CONTAINER ID     IMAGE           COMMAND      CREATED            STATUS
54f4984ed6a8     hello-world     "/hello"     20 seconds ago     Exited (0) 19 seconds ago
```

### Define a container with `Dockerfile`.

Dockerfile defines what goes on in the environment inside your container. Access to resources like networking interfaces and
disk drives is virtualized inside this environment, which is isolated from the rest of your system,
so you need to map ports to the outside world, and be specific about what files you want
to “copy in” to that environment. However, after doing that, you can expect that the build of
your app defined in this Dockerfile behaves exactly the same wherever it runs.

```Dockerfile
# Use an official Python runtime as a parent image
FROM python:2.7-slim

# Set the working directory to /app
WORKDIR /app

# Copy the current directory contents into the container at /app
COPY . /app

# Install any needed packages specified in requirements.txt
RUN pip install --trusted-host pypi.python.org -r requirements.txt

# Make port 80 available to the world outside this container
EXPOSE 80

# Define environment variable
ENV NAME World

# Run app.py when the container launches
CMD ["python", "app.py"]
```

### Build the app.

Now run the build command. This creates a Docker image, which we’re going to name using
the `--tag` option. Use `-t` if you want to use the shorter option.

```bash
docker build --tag=friendlyhello .
```

Where is your built image? It’s in your machine’s local Docker image registry:

```bash
$ docker image ls

REPOSITORY            TAG                 IMAGE ID
friendlyhello         latest              326387cea398
```

Note how the tag defaulted to latest. The full syntax for the tag option would be something like `--tag=friendlyhello:v0.0.1`.

### Run the app.

Run the app, mapping your machine’s port 4000 to the container’s published port 80 using `-p`:

```bash
docker run -p 4000:80 friendlyhello
```

Now let’s run the app in the background, in detached mode:

```bash
docker run -d -p 4000:80 friendlyhello
```

You get the long container ID for your app and then are kicked back to your terminal. Your container is running in the background.
You can also see the abbreviated container ID with docker container ls (and both work interchangeably when running commands):

```bash
$ docker container ls
CONTAINER ID        IMAGE               COMMAND             CREATED
1fa4ab2cf395        friendlyhello       "python app.py"     28 seconds ago
```

Notice that CONTAINER ID matches what’s on `http://localhost:4000`.

Now use docker container stop to end the process, using the `CONTAINER ID`, like so:

```bash
docker container stop 1fa4ab2cf395
```

### Share your image.

To demonstrate the portability of what we just created, let’s upload our built image and run it somewhere else.
After all, you need to know how to push to registries when you want to deploy containers to production.

A registry is a collection of repositories, and a repository is a collection of images—sort of like a GitHub repository,
except the code is already built. An account on a registry can create many repositories. The `docker` CLI uses Docker’s public registry by default.

Log in with your Docker ID
If you don’t have a Docker account, sign up for one at `hub.docker.com`. Make note of your username.

Log in to the Docker public registry on your local machine.

```bash
$ docker login
```

### Tag the image.
The notation for associating a local image with a repository on a registry is `username/repository:tag`.
The tag is optional, but recommended, since it is the mechanism that registries use to give Docker images a version.
Give the repository and tag meaningful names for the context, such as `get-started:part2`.
This puts the image in the `get-started` repository and tags it as `part2`.

Now, put it all together to tag the image. Run `docker tag image` with your username, repository,
and tag names so that the image uploads to your desired destination. The syntax of the command is:

```bash
docker tag image username/repository:tag
```

For example:

```bash
docker tag friendlyhello gordon/get-started:part2
```

Run docker image ls to see your newly tagged image.

```bash
$ docker image ls

REPOSITORY               TAG                 IMAGE ID            CREATED             SIZE
friendlyhello            latest              d9e555c53008        3 minutes ago       195MB
gordon/get-started         part2               d9e555c53008        3 minutes ago       195MB
python                   2.7-slim            1c7128a655f6        5 days ago          183MB
...
```

### Publish the image.

Upload your tagged image to the repository:

```bash
docker push username/repository:tag
```

Once complete, the results of this upload are publicly available.
If you log in to Docker Hub, you see the new image there, with its pull command.

### Pull and run the image from the remote repository.

From now on, you can use `docker run` and run your app on any machine with this command:

```bash
docker run -p 4000:80 username/repository:tag
```

If the image isn’t available locally on the machine, Docker pulls it from the repository.

```bash
$ docker run -p 4000:80 gordon/get-started:part2
Unable to find image 'gordon/get-started:part2' locally
part2: Pulling from gordon/get-started
10a267c67f42: Already exists
f68a39a6a5e4: Already existss
9beaffc0cf19: Already exists
3c1fe835fb6b: Already exists
4c9f1fa8fcb8: Already exists
ee7d8f576a14: Already exists
fbccdcced46e: Already exists
Digest: sha256:0601c866aab2adcc6498200efd0f754037e909e5fd42069adeff72d1e2439068
Status: Downloaded newer image for gordon/get-started:part2
 * Running on http://0.0.0.0:80/ (Press CTRL+C to quit)
```

No matter where `docker run` executes, it pulls your image, along with Python and all the dependencies from `requirements.txt`, and runs your code.
It all travels together in a neat little package, and you don’t need to install anything on the host machine for Docker to run it.