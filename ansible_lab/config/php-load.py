import requests
from multiprocessing import Process

def loadHttpServer1():
    for x in range(1,100000,1):
        try:
            r1 = requests.get("http://10.145.0.11/virtual1/info.php")
            r1.content
        except requests.exceptions.ConnectionError:
            r1.status_code = "Connection refused"
        print "Status code 1="+str(r1.status_code)+"\n"
    print r1.headers
    print r1.content

def loadHttpServer2():
    for y in range(1,100000,1):
        try:
            r2 = requests.get("http://10.145.0.11/virtual1/info.php")
            r2.content
        except requests.exceptions.ConnectionError:
            r2.status_code = "Connection refused"
        print "Status code 2="+str(r2.status_code)+"\n"
    print r2.headers
    print r2.content

def loadHttpServer3():
    for z in range(1,100000,1):
        try:
            r3 = requests.get("http://10.145.0.11/virtual1/info.php")
            r3.content
        except requests.exceptions.ConnectionError:
            r3.status_code = "Connection refused"
        print "Status code 3="+str(r3.status_code)+"\n"
    print r3.headers
    print r3.content

if __name__ == "__main__":
    p1 = Process(target=loadHttpServer1)
    p1.start()
    p2 = Process(target=loadHttpServer2)
    p2.start()
    p3 = Process(target=loadHttpServer3)
    p3.start()

